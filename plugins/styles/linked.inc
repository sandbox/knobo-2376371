<?php
/**
 * @file
 * Linked panel style
 */

// Plugin definition.
$plugin = array(
  'title'            => t('Linked'),
  'description'      => t('Wrap regions and panes with a link to a context'),
  'render region'    => 'linked_panel_style_render_region',
  'render pane'      => 'linked_panel_style_render_pane',
  'settings form'    => 'linked_panel_style_settings_form',
  'single'           => TRUE,
  'required context' => array(
    new ctools_context_required(t('Node'), 'node'),
  ),
);

/**
 * Linked panel settings form.
 */
function linked_panel_style_settings_form($form, &$form_state) {
  $node_list = array();

  foreach ($form_state->context as $key => $val) {
    $node_list[$key] = $val->identifier;
  }

  $default_value = array();

  if (isset($form['link'])) {
    $default_value = array($form['link']);
  }

  $form['link'] = array(
    '#type' => 'select',
    '#title' => t('Context'),
    '#options' => $node_list,
    '#default_value' => $default_value,
  );
  return $form;
}


/**
 * Validate handler for linked panel settings form.
 */
function linked_panel_settings_form_validate($form, &$form_state) {
  if (empty($form_state['values']['link'])) {
    form_set_error('link', t('You must select a context'));
  }
}

/**
 * Submit handler for linked panel settings form.
 */
function linked_panel_settings_form_submit($form, &$form_state) {
  $form_state['conf']['link'] = $form_state['values']['link'];
}
